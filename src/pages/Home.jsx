import React from 'react'
import ColHead from '../components/ColHead'
import Navbar from '../components/Navbar'
import MainMatrix from '../components/MainMatrix'

const Home = () => {
  return (
    <div>
        <Navbar/>
        <MainMatrix/>
    </div>
  )
}

export default Home