import React from 'react'
import { useState, useEffect } from "react";
import styled from 'styled-components'
import '../app.css'
import { marketData } from './data';
import axios from 'axios';

const Container = styled.div`
    padding: 0px 2px;
    display: flex;
    align-items: center;
    justify-content: center;
`
const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    flex: 3;
    width: 100%;
    background-color: gray;
`

const Col = styled.div`
    margin-right: 4px;
    margin-left: 4px;
    border: 1px solid;
    height: 100px;
    width: 100px;
    align-items: center;
    justify-content: center;
    display: flex;
    flex-direction: column;
`
const ColBut = styled.div`
    border: 1px solid;
    height: 100px;
    width: 100px;
    align-items: center;
    justify-content: center;
    display: flex;
    flex-direction: column;
`

const Top = styled.h5`
    color: white;
`
const Bot = styled.h5`
`
const Mid = styled.h2`
    color: white;
`
const NameHolder = styled.div`
    flex: 1;
    align-items: center;
    justify-content: center;
    width: 100%;
    text-align: center;
`
const Name = styled.h2`
    font-weight: bolder;
`

const Button = styled.button`
    padding: 2px;
    margin-right: 0px;
    cursor: pointer;
`
const Row = styled.div`
    padding: 100px 500px 2px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`
const ContainerColumn = styled.div`
    padding: 0px 2px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`
const TopC = styled.div`
    color: red;
    align-items: center;
    justify-content: center;
    width: 100%;
    align-items: center;
    justify-content: center;
    background-color: black;
    text-align: center;
    font-size: smaller;
    font-weight: bold;
    flex: 1;
`
const BotC = styled.div`
    align-items: center;
    justify-content: center;
    width: 100%;
    color: rgb(55, 231, 55);
    align-items: center;
    justify-content: center;
    background-color: black;
    text-align: center;
    font-size: smaller;
    font-weight: bold;
    flex: 1;
`
const MidC = styled.div`
    padding: 16px 0px;
    width: 100%;
    color: black;
    align-items: center;
    justify-content: center;
    background-color: rgba(${(props) => (0 - props.middleValue) * 255},${(props) => props.middleValue * 255},0,0.5);
    text-align: center;
    font-size: x-large;
    font-weight: bolder;
    flex: 3;
`

const Select = styled.select`
    width: 100%;
    //padding: 10px;
    //margin-right: 20px;
    //color: transparent;
    font-size: 20px;
`
const Option = styled.option``;

const MainMatrix = () => {
    const [activeButton, setActiveButton] = useState(0);
    const cols = marketData;
    const [fetchStarted, setFetchStarted] = useState(0);
    const [unocoin,setUnocoin] = useState([[]]);
    const [zeb,setZeb] = useState([[]]);
    const [bisq,setBisq] = useState([[]]);

        

        useEffect(() => {
            setInterval(() => {
                const getMarkets = async () => {
                    try {
                        const res = await axios.get("http://localhost:5000/trades").then(res =>{
                            setUnocoin(res.data.unocoinApiBtcMarkets);
                            setZeb(res.data.zebApiBtcMarkets);
                            setBisq(res.data.bisqApiBtcMarkets);
                        });
                    } catch (error) {
                        console.log(error);
                    } finally{
                        setFetchStarted(1);
                    }
                };
                getMarkets();
            }, 100);
        }, []);

  return( 
    <Row> 

    <Container>
        <Col>
        <Select>
            <Option>BTC/INR</Option>
            <Option>ETH/INR</Option>
            <Option>SOL/INR</Option>
        </Select>
        <MidC><u>BTC</u><br></br>INR</MidC>
        </Col>
        <Button onClick={() => setActiveButton(1)}
            className={activeButton === (1) ? "activeR" : "deactive"}
        >
        <ColBut>
        <NameHolder className = 'redBG'><Name>UNOCOIN</Name></NameHolder>
            <Wrapper>
                <Top>{(unocoin[0].sell-unocoin[0].buy)}</Top>
                <Mid><colName>{(unocoin[0].buy)}</colName></Mid>
                <Bot className = 'redTEXT'>{(unocoin[0].sell)}</Bot>
            </Wrapper>
        </ColBut>
        </Button>
        <Button onClick={() => setActiveButton(2)}
            className={activeButton === (2) ? "activeR" : "deactive"}
        >
        <ColBut>
        <NameHolder className = 'redBG'><Name>ZEB</Name></NameHolder>
            <Wrapper>
                <Top>{(zeb[0].sell-zeb[0].buy).toFixed(2)}</Top>
                <Mid><colName>{(zeb[0].buy)}</colName></Mid>
                <Bot className = 'redTEXT'>{(zeb[0].sell)}</Bot>
            </Wrapper>
        </ColBut>
        </Button>
    </Container>
    
    <ContainerColumn>
    <Container>
        <Button onClick={() => setActiveButton(1)}
            className={activeButton === (1) ? "activeG" : "deactive"}
        >
        <ColBut>
            <Wrapper>
                <Top>{(unocoin[0].sell-unocoin[0].buy).toFixed(2)}</Top>
                <Mid><colName>{(unocoin[0].buy)}</colName></Mid>
                <Bot className = 'greenTEXT'>{(unocoin[0].buy)}</Bot>
            </Wrapper>
        <NameHolder className = 'greenBG'><Name>UNOCOIN</Name></NameHolder>
        </ColBut>
        </Button>
    <Col>
        <Wrapper>
            <TopC>{(unocoin[0].sell)}</TopC>
            <MidC middleValue={(((unocoin[0].sell-unocoin[0].buy)/unocoin[0].buy)*100).toFixed(3)}><colName>{(((unocoin[0].sell-unocoin[0].buy)/unocoin[0].buy)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(unocoin[0].buy)}</BotC>
        </Wrapper>
    </Col>
    <Col>
        <Wrapper>
            <TopC>{(zeb[0].sell)}</TopC>
            <MidC middleValue={(((zeb[0].sell-unocoin[0].buy)/unocoin[0].buy)*100).toFixed(3)}><colName>{(((zeb[0].sell-unocoin[0].buy)/unocoin[0].buy)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(unocoin[0].buy)}</BotC>
        </Wrapper>
    </Col>
    </Container>
    <Container>
        <Button onClick={() => setActiveButton(2)}
            className={activeButton === (2) ? "activeG" : "deactive"}
        >
        <ColBut>
            <Wrapper>
                <Top>{(zeb[0].sell-zeb[0].buy).toFixed(2)}</Top>
                <Mid><colName>{(zeb[0].buy)}</colName></Mid>
                <Bot className = 'greenTEXT'>{(zeb[0].buy)}</Bot>
            </Wrapper>
        <NameHolder className = 'greenBG'><Name>ZEB</Name></NameHolder>
        </ColBut>
        </Button>
    <Col>
        <Wrapper>
            <TopC>{(unocoin[0].sell)}</TopC>
            <MidC middleValue={(((unocoin[0].sell-zeb[0].buy)/zeb[0].buy)*100).toFixed(3)}><colName>{(((unocoin[0].sell-zeb[0].buy)/zeb[0].buy)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(zeb[0].buy)}</BotC>
        </Wrapper>
    </Col>
    <Col>
        <Wrapper>
            <TopC>{(zeb[0].sell)}</TopC>
            <MidC middleValue={(((zeb[0].sell-zeb[0].buy)/zeb[0].buy)*100).toFixed(3)}><colName>{(((zeb[0].sell-zeb[0].buy)/zeb[0].buy)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(zeb[0].buy)}</BotC>
        </Wrapper>
    </Col>
    </Container>
    </ContainerColumn>

    </Row>
  )
}

export default MainMatrix