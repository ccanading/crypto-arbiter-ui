import React from 'react'
import { useState, useEffect } from "react";
import styled from 'styled-components'
import '../app.css'
import { marketData } from './data';
import axios from 'axios';

const Container = styled.div`
    padding: 0px 2px;
    display: flex;
    align-items: center;
    justify-content: center;
`
const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    flex: 3;
    width: 100%;
    background-color: gray;
`

const Col = styled.div`
    margin-right: 4px;
    margin-left: 4px;
    border: 1px solid;
    height: 100px;
    width: 100px;
    align-items: center;
    justify-content: center;
    display: flex;
    flex-direction: column;
`
const ColBut = styled.div`
    border: 1px solid;
    height: 100px;
    width: 100px;
    align-items: center;
    justify-content: center;
    display: flex;
    flex-direction: column;
`

const Top = styled.h5`
    color: white;
`
const Bot = styled.h5`
`
const Mid = styled.h2`
    color: white;
`
const NameHolder = styled.div`
    flex: 1;
    align-items: center;
    justify-content: center;
    width: 100%;
    text-align: center;
`
const Name = styled.h2`
    font-weight: bolder;
`

const Button = styled.button`
    padding: 2px;
    margin-right: 0px;
    cursor: pointer;
`
const Row = styled.div`
    padding: 100px 500px 2px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`
const ContainerColumn = styled.div`
    padding: 0px 2px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`
const TopC = styled.div`
    color: red;
    align-items: center;
    justify-content: center;
    width: 100%;
    align-items: center;
    justify-content: center;
    background-color: black;
    text-align: center;
    font-size: smaller;
    font-weight: bold;
    flex: 1;
`
const BotC = styled.div`
    align-items: center;
    justify-content: center;
    width: 100%;
    color: rgb(55, 231, 55);
    align-items: center;
    justify-content: center;
    background-color: black;
    text-align: center;
    font-size: smaller;
    font-weight: bold;
    flex: 1;
`
const MidC = styled.div`
    padding: 16px 0px;
    width: 100%;
    color: black;
    align-items: center;
    justify-content: center;
    background-color: rgba(${(props) => (0 - props.middleValue) * 255},${(props) => props.middleValue * 255},0,0.5);
    text-align: center;
    font-size: x-large;
    font-weight: bolder;
    flex: 3;
`

const Select = styled.select`
    width: 100%;
    //padding: 10px;
    //margin-right: 20px;
    //color: transparent;
    font-size: 20px;
`
const Option = styled.option``;

const ColHead = () => {
    const [activeButton, setActiveButton] = useState(0);
    const cols = marketData;
    const [fetchStarted, setFetchStarted] = useState(0);
    const [unocoin,setUnocoin] = useState([[]]);
    const [zeb,setZeb] = useState([[]]);
    const [bisq,setBisq] = useState([[]]);

        

        useEffect(() => {
            setInterval(() => {
                const getMarkets = async () => {
                    try {
                        const res = await axios.get("http://localhost:5000/trades").then(res =>{
                            setUnocoin(res.data.unocoinApiBtcMarkets);
                            setZeb(res.data.zebApiBtcMarkets);
                            setBisq(res.data.bisqApiBtcMarkets);
                        });
                    } catch (error) {
                        console.log(error);
                    } finally{
                        setFetchStarted(1);
                    }
                };
                getMarkets();
            }, 1000);
        }, []);

        
  return(
    <Row> 
      {unocoin[0].sell}
    <Container>
        <Col>
        <Select>
            <Option>BTC/INR</Option>
            <Option>ETH/INR</Option>
            <Option>SOL/INR</Option>
        </Select>
        <MidC><u>BTC</u><br></br>INR</MidC>
        
        </Col>
        {marketData.map(item=>(
        <Button onClick={() => setActiveButton(item.id)}
            className={activeButton === (item.id) ? "activeR" : "deactive"}
        >
        <ColBut>
        <NameHolder className='redBG'><Name>{item.name}</Name></NameHolder>
            <Wrapper>
                <Top>{(item.ask-item.bid).toFixed(8)}</Top>
                <Mid><colName>{(item.bid).toFixed(2)}</colName></Mid>
                <Bot className='redTEXT'>{(item.ask).toFixed(2)}</Bot>
            </Wrapper>
        </ColBut>
        </Button>
        ))}
    </Container>
    <ContainerColumn>
    {marketData.map(item=>(
    <Container>
        <Button onClick={() => setActiveButton(item.id)}
            className={activeButton === (item.id) ? "activeG" : "deactive"}
        >
        <ColBut>
            <Wrapper>
                <Top>{(item.ask-item.bid).toFixed(8)}</Top>
                <Mid><colName>{(item.bid).toFixed(2)}</colName></Mid>
                <Bot className = 'greenTEXT'>{(item.bid).toFixed(2)}</Bot>
            </Wrapper>
        <NameHolder className = 'greenBG'><Name>{item.name}</Name></NameHolder>
        </ColBut>
        </Button>
    <Col>
        <Wrapper>
            <TopC>{(cols[0].ask).toFixed(2)}</TopC>
            <MidC middleValue={(((cols[0].ask-item.bid)/item.bid)*100).toFixed(3)}><colName>{(((cols[0].ask-item.bid)/item.bid)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(item.bid).toFixed(2)}</BotC>
        </Wrapper>
    </Col>
    <Col>
        <Wrapper>
            <TopC>{(cols[1].ask).toFixed(2)}</TopC>
            <MidC middleValue={(((cols[1].ask-item.bid)/item.bid)*100).toFixed(3)}><colName>{(((cols[1].ask-item.bid)/item.bid)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(item.bid).toFixed(2)}</BotC>
        </Wrapper>
    </Col>
    <Col>
        <Wrapper>
            <TopC>{(cols[2].ask).toFixed(2)}</TopC>
            <MidC middleValue={(((cols[2].ask-item.bid)/item.bid)*100).toFixed(3)}><colName>{(((cols[2].ask-item.bid)/item.bid)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(item.bid).toFixed(2)}</BotC>
        </Wrapper>
    </Col>
    <Col>
        <Wrapper>
            <TopC>{(cols[3].ask).toFixed(2)}</TopC>
            <MidC middleValue={(((cols[3].ask-item.bid)/item.bid)*100).toFixed(3)}><colName>{(((cols[3].ask-item.bid)/item.bid)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(item.bid).toFixed(2)}</BotC>
        </Wrapper>
    </Col>
    <Col>
        <Wrapper>
            <TopC>{(cols[4].ask).toFixed(2)}</TopC>
            <MidC middleValue={(((cols[4].ask-item.bid)/item.bid)*100).toFixed(3)}><colName>{(((cols[4].ask-item.bid)/item.bid)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(item.bid).toFixed(2)}</BotC>
        </Wrapper>
    </Col>
    <Col>
        <Wrapper>
            <TopC>{(cols[5].ask).toFixed(2)}</TopC>
            <MidC middleValue={(((cols[5].ask-item.bid)/item.bid)*100).toFixed(3)}><colName>{(((cols[5].ask-item.bid)/item.bid)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(item.bid).toFixed(2)}</BotC>
        </Wrapper>
    </Col>
    <Col>
        <Wrapper>
            <TopC>{(cols[6].ask).toFixed(2)}</TopC>
            <MidC middleValue={(((cols[6].ask-item.bid)/item.bid)*100).toFixed(3)}><colName>{(((cols[6].ask-item.bid)/item.bid)*100).toFixed(3)}</colName></MidC>
            <BotC className='greenTEXT'>{(item.bid).toFixed(2)}</BotC>
        </Wrapper>
    </Col>
    </Container>
        ))}
    </ContainerColumn>
    </Row>
  )
}

export default ColHead