import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
    height: 60px;
    background-color: lightblue;
`
const Wrapper = styled.div`
    padding: 10px 20px;
    display: flex;
    justify-content: space-between; 
`
const Left = styled.div`
    flex: 2;
`
const Center = styled.div`
    flex: 4;
`
const Right = styled.div`
    flex: 4;
`

const Logo = styled.h1`
    font-weight: bold;
`

const Navbar = () => {
  return (
    <Container>
        <Wrapper>
            <Left><Logo>APPNAME</Logo></Left>
            <Center></Center>
            <Right></Right>
        </Wrapper> 
    </Container>
  )
}

export default Navbar