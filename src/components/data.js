export const marketData = [
    {
        id: 1,
        name: "GDAX",
        bid: 7558.51,
        ask: 7558.52,
    },
    {
        id: 2,
        name: "BITS",
        bid: 7561.41,
        ask: 7561.42,
    },
    {
        id: 3,
        name: "CBNK",
        bid: 7308.06,
        ask: 7524.00,
    },
    {
        id: 4,
        name: "CXIO",
        bid: 7625.40,
        ask: 7624.20,
    },
    {
        id: 5,
        name: "EXMO",
        bid: 7543.50,
        ask: 7560.00,
    },
    {
        id: 6,
        name: "BTRX",
        bid: 7589.13,
        ask: 7566.07,
    },
    {
        id: 7,
        name: "GMNI",
        bid: 7560.41,
        ask: 7556.95,
    }
]